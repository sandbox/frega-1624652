<?php
/**
 * Implementation of hook_default_rules_configuration().
 */
function subpub_default_rules_configuration() {
  $items = array();
  $items['rules_subpub_new_node'] = entity_import('rules_config', '{ "rules_rules_subpub_new_node" : {
      "LABEL" : "rules_subpub_new_node",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "subpub" ],
      "REQUIRES" : [ "rules", "subpub" ],
      "ON" : [ "node_insert" ],
      "DO" : [
        { "subsub_say" : {
            "channel" : "#test",
            "message" : "[node:title] ([node:url]) has been created by [site:current-user]."
          }
        },
        { "drupal_message" : { "message" : "Notified IRC!" } }
      ]
    }
  }');
  $items['rules_rules_subpub_message_echo'] = entity_import('rules_config', '{ "rules_rules_subpub_message_echo" : {
      "LABEL" : "rules_subpub_message_echo",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "subpub" ],
      "REQUIRES" : [ "rules", "subpub", "message" ],
      "ON" : [ "message_insert" ],
      "IF" : [
        { "data_is" : { "data" : [ "message:type" ], "value" : "subpub_message" } }
      ],
      "DO" : [
        { "subsub_say" : { "channel" : "#test", "message" : "Echo: [message:text]" } }
      ]
    }
  }');
  $items['rules_rules_subpub_message_broadcast'] = entity_import('rules_config', '{ "rules_rules_subpub_message_broadcast" : {
      "LABEL" : "rules_subpub_message_broadcast",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "subpub" ],
      "REQUIRES" : [ "rules", "dnode_faye_notify", "message" ],
      "ON" : [ "message_insert" ],
      "IF" : [
        { "data_is" : { "data" : [ "message:type" ], "value" : "subpub_message" } }
      ],
      "DO" : [
        { "dnode_faye_notify_broadcast" : { "subject" : "IRC Message!", "message" : "[message:text]" } }
      ]
    }
  }');
  return $items;
}