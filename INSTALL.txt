You will need to have the dnode-module properly installed and configured.

1) Note: you need to activate either dnode_php or dnode_http!
2) Do not forget to install the dependencies

  cd ssjs ; npm install

3) For testing purposes you might want to install a local IRC server. For
Debian/Ubuntu this works without much ado:

  sudo apt-get install ircd-irc2
