DrupalDnode = global.DrupalDnode;
var config = DrupalDnode.getModuleConfig('subpub');
var irc = require('irc'), c = require('irc-colors');
var bot = new irc.Client(
  config.irc_server, config.bot_name, {
  debug: config.debug,
  channels: config.irc_channels
});
//
bot.addListener('message', function (from, channel, message, more) {
  DrupalDnode.connectByServerId('dnodeq', function(remote, conn) {
    remote.sendMessageToDrupal('subpub', {
      from: from,
      channel: channel,
      message: message,
      data: more,
      timestamp: +( new Date() )
    }, {}, function(err, data) {
      console.log('Sent to drupal - %s => %s: %s', from, channel, message);
      conn.end();
    });
  });
});

exports.say = function(channel, message, cb) {
  // strip html tags
  message = message.replace(/(<([^>]+)>)/ig,"");
  console.log('SAY %s in %s', channel);
  bot.say(channel, message);
  cb(null);
}