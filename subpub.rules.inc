<?php
/**
 * @file
 * Rules file for the subpub module.
 */

/**
 * Implements hook_rules_event_info().
 */
function subpub_rules_action_info() {
  return array(
    'subsub_say' => array(
      'label' => t('subpub: say something'),
      'parameter' => array(
        'channel' => array(
          'type' => 'text',
          'label' => t('Channel'),
          'optional' => FALSE,
        ),
        'message' => array(
          'type' => 'text',
          'label' => t('Message'),
          'optional' => TRUE,
        ),
        'callback' => array(
          'type' => 'text',
          'label' => t('PHP function name to handle the callback (optional)'),
          'optional' => TRUE,
        ),
      ),
      'group' => t('subpub - irc'),
      'base' => 'subpub_rules_say',
      'callbacks' => array(),
    ),
  );
}

/**
 * Rules action callback to say something in irc.
 */
function subpub_rules_say($channel, $message, $cb = NULL, $element = NULL) {
  dnode_rpc('subpub', 'say', array($channel, $message), $cb ? $cb : FALSE);
}
